from selenium import webdriver
import getpass

driver = webdriver.Chrome()

def pilihan_menu():
    print('\n')
    print('** Selamat Datang Di Otomasiz Sosz/ **\n')
    print('**************************************\n')
    print('Pilih Sosz Yang Anda Inginkan\n')
    print('1. Facebook\n')
    print('2. Instagram\n')
    print('3. Twitter\n')
    pertanyaan = input('Masukkan Pilihan :')
    while (pertanyaan not in ['1','2','3']):
        print('Tidak Ada Dalam Pilihan')
        pertanyaan = input('Masukkan Pilihan :')
        print()
    return pertanyaan
        
def panggil_fac():
    driver.get('https://www.facebook.com/')
    email = input('Masukkna Email Anda :')
    password = getpass.getpass('Masukkan Password Anda :')
    driver.find_element_by_id('email').send_keys(email)
    driver.find_element_by_id('pass').send_keys(password)
    driver.find_element_by_id('loginbutton').click()

def panggil_ing():
    driver.get('https://www.instagram.com/accounts/login/?hl=en&source=auth_switcher')
    email = input('Masukkna Email/Username/No Hp Anda :')
    password = getpass.getpass('Masukkan Password Anda :')
    driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/article/div/div[1]/div/form/div[2]/div/label/input').send_keys(email)
    driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/article/div/div[1]/div/form/div[3]/div/label/input').send_keys(password)
    driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/article/div/div[1]/div/form/div[4]/button').click()

def panggil_tw():
    driver.get('https://twitter.com/login')
    email = input('Masukkna Email/Username/No Hp Anda :')
    password = getpass.getpass('Masukkan Password Anda :')
    driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[1]/input').send_keys(email)
    driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[2]/input').send_keys(password)
    driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/div[2]/button').click()

def panggil_func(pertanyaan):
    if pertanyaan == '1':
        panggil_fac()

    elif pertanyaan == '2':
        panggil_ing()

    elif pertanyaan == '3': 
        panggil_tw()

def main():
	pertanyaan=pilihan_menu()
	panggil_func(pertanyaan)
	print()


if __name__=='__main__':
	main()

 

